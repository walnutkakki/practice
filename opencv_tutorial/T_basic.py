#numpy import를 굳이 할 필요가 없네. 왜인가.... img의 자료형은 ndarray인데.. 왜? (의문)
#numpy란 ...
#import란 ... 
#import numpy as np

import cv2
#showImage라는 함수를 만들어 이미지를 읽고 화면에 나타내게 한다.

def showImage():
#파일 불러오기. \가 아닌/를 쓰는 것. 파일 경로를 불러오는건 어떻게 저장될까?
#imread(읽을 파일 경로, 읽는 방식) - 이미지 파일을 읽을 객체를 리턴하는 함수. 파일이 없거나 읽지 못하면 null값을 추출
#기본적으로 bmp 파일로 읽어온다. 하지만 jpg, png, tiff도 가능함. 내재된 라이브라리(libjpeg, libpng, libtiff, and libjasper) 덕분에.
#디코딩된 이미지는 B G R 채널 순서로 저장됨.
#img = 'C:/practice/img1.jpg' 이렇게 하면 이 경로 주소를 저장하는 것이므로 string 형태로 저장됨
#IMREAD_COLOR(컬러이미지, 투명 무시, 정수값 1)
#IMREAD_GRAYSCALE(흑백, 정수값 0)
#IMREAD_UNCHANGED(알파채널{이미지 선택영역 수정할 수 있는 곳}}} 포함하여 그대로, 정수값  -1)
#img는 print(type(img)) 를 하면 ndarray의 자료로 받아진다고 나옴
#ndarray는 같은 종류의 데이터만을 배열로 받는다.
#출:http://yujuwon.tistory.com/entry/NumPy
    img = cv2.imread('C:/practice/img1.jpg', cv2.IMREAD_COLOR)

# 이미지가 열릴 때 창의 성격, normal은 원본 사이즈, 크기조절 가능, autosize는 원본으로 고정
    cv2.namedWindow('img1', cv2.WINDOW_NORMAL)

#imread로 읽어들인 객체를 보여주는 역할을 함. (열릴 때 창 이름, 열어올 객체)
#img가 부호없는 8비트일 때(그대로 표시), 부호없는 16비트 혹은 32비트 정수 일 때(256으로 픽셀을 나눔, 0,255*256은 0,255 가 되) 
#32비트 실수(부동소수점)일 때(255를 곱해, 0,1이 0,255가 됨) --> 8비트가 디폴트니 맞춰주겠다라는 의도인 듯 하다.
#namedwindow에 window_autosize를 하지 않았다면 
#
    cv2.imshow('img1',img)

#이미지 표시 후 키보드 입력 까지 대기시간, 0이면 입력까지 기다리라는거, 단위는 ms인 것. 
#이 때 cv2.waitKey 의 반환값은 누른 키보드의 값임. esc는 27이당.
    cv2.waitKey(0)
#윈도우 제거 -> 키보드 누르면 꺼진다 라는 말.
    cv2.destroyAllWindows()

showImage()

