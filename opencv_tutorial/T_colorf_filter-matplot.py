import cv2
import matplotlib.pyplot as plt
#plt로 줄여서 쓰는 것
#출 : http://crmn.tistory.com/55
imgBGR = cv2.imread('C:/practice/img2.jpg', cv2.IMREAD_COLOR)
#cvtColor는 변환 함수임. 우리가 처음에 이미지를 불러올 때 데이터의 구성은 칼라로 불러올 시 24bit로 처리하는데 
#픽셀이 가지는 값은 B/G/R의 순서로 되어있다. 
#cvtColor(source,type) 순서인데 BGR을 RGB(BGR2RGB), GRAY(BGR2GRAY), HSV(BGR2HSV)
#의 type가 있다.
imgRGB = cv2.cvtColor(imgBGR, cv2.COLOR_BGR2RGB)
#붉은 색의 영상을 보여주기 위해, 변환된 imgRGB를 copy함
#copy에는 deep/shallow 두 종류가 있음.
#shallow 카피는 .copy()의 형식. 그냥 ㅁ=ㅅ의 방식으로
#리스트를 카피할 시 주소까지 다 공유해서 ㅅ의 리스트 내용을 바꾸면 ㅁ도 같이 바뀌게 됨
#deepcopy는 copy.deepcopy()의 형태로 이를 사용하면 위의 문제는 해결됨.
imgR = imgRGB.copy()
#이 때, 데이터는 [R,G,B]의 형태로 저장되어있기 때문에 [:,:,1]이라고 하면 [0,1,2] 위치에서 1 위치에 있는 값들을 정하는 것이므로
#예를 들면 [124,0,151352]가 될 것이다
#R은 0, G는 1, B는 2이기 때문에 1,2가 0이 되면 R만 남는다. 아래의 imgG, imgB도 같은 원리.
#R 값만 남게 함
imgR[:,:,1] = 0
imgR[:,:,2] = 0
#G 값만 남게 함
imgG = imgRGB.copy()
imgG[:,:,0] = 0
imgG[:,:,2] = 0
#B 값만 남게 함
imgB = imgRGB.copy()
imgB[:,:,0] = 0
imgB[:,:,1] = 0
#matplotlib의 subplot은 그래프를 여러개 보여주는 기능임
#subplot(행 수, 열 수, 순서)의 구성으로 되어있음.
plt.subplot(1, 4, 1)
plt.imshow(imgRGB)

plt.subplot(1, 4, 2)
plt.imshow(imgR)

plt.subplot(1, 4, 3)
plt.imshow(imgG)

plt.subplot(1, 4, 4)
plt.imshow(imgB)
#show가 있어야 보여지는 것이다
plt.show()