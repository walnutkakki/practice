import numpy as np
import matplotlib.pyplot as plt
import cv2
#영상 불러오기
capt = cv2.VideoCapture('c:/practice/vod2.mp4')
#영상으로 만들어진 이미지 불러오기
img1 = cv2.imread('c:/practice/images/frame1.jpg', cv2.IMREAD_COLOR)
img2 = cv2.imread('c:/practice/images/frame2.jpg', cv2.IMREAD_COLOR)
#프레임 저장하는 부분 시작
#frame을 나눌 count를 정한다.
count = 0


#while: 이랑 같은거지. isOpened는 video가 open 됐을 때 true를 반환함.
while(capt.isOpened()):
    ret, frame = capt.read()
    
    if ret:
        
        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        #imwrite로 저장함. count로 영상 종료까지 프레임 수를 알 수 있음. while루프는 영상 끝날 때 까지 계속 하는거임.
        #imwrite(,)
        cv2.imwrite('c:/practice/images/grayframe%d.jpg' % count, rgb)
        print("save %d frame" % count)
        count+=1
    else: 
        break
capt.release()
#프레임 저장하는 부분 끝

#뽑아낸 프레임에서 img1부분 matplotlib로 출력
#plt.subplot(1, 2, 1)
#plt.imshow(img1)

#img를 불러와서 B값만 남기고 나머지 싹 지워 표시하는 부분, 이때 처음 가져온 img2는 BGR순서이므로 BGR로 쓰던지 하자 그럼

img2[:,:,0] = 0
img2[:,:,1] = 0
cv2.imshow('img2',img2)
#plt.subplot(1, 2, 2)
#plt.imshow(img2)
#이 과정에서 계속 red 값만 남겨져서 저장이 되어버림... imwrite할 때 RGB가 다시 BGR이 되어버리는 듯 함 어떻게하지?
#이유 : imwrite는 bgr순서로 저장을 하게되는데, 그리하여 cv2.imshow를 이용해 볼 때는 bgr순서로의 데이터를 볼 수 있다.
#하지만 matplot에서는 rgb순서로 정리가 되어져 보여지는 듯 함. cvtcolor를 통해 컨버팅을 한 후에 imwrite가 제대로 안 되었으니
#이때 imwrite의 두번째 설정, 저장할 변수에 cv2.cvtColor(변수,옵션) 을 사용하면 RGB로 저장할 수 있음. -> 그 결과 red로 저장 안되고 blue로 저장이 됨
#cv2.imwrite('c:/practice/images/frame2B.jpg' , img2)
cv2.imwrite('c:/practice/images/betaframe2B.jpg' , cv2.cvtColor(img2, cv2.COLOR_BGR2RGB))
print("save complete")

plt.show()