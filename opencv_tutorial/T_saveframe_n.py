import numpy as np
import cv2

capt = cv2.VideoCapture('c:/practice/vod2.mp4')
#frame을 나눌 count를 정한다.
count = 0
#while: 이랑 같은거지. isOpened는 video가 open 됐을 때 true를 반환함.
while(capt.isOpened()):
    ret, frame = capt.read()
    
    if ret:
    
        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        #get은
        #여기서 n은 20이다.
        if(int(capt.get(1)) % 20 == 0):
            print('Saved frame number : ' + str(int(capt.get(1))))
            cv2.imwrite('c:/practice/images/20frame%d.jpg' % count, rgb)
            print('Saved frame%d.jpg' % count)
            count += 1

    else: 
        break
capt.release()
