import numpy as np
import cv2

#video를 불러오는 함수임.
#class는 cv2.VideoCapture였음. 
#cv2.VideoCapture('불러올 파일 경로' or 0(연결된 카메라가 하나일 때) or device명)
capt = cv2.VideoCapture('c:/practice/vod1.mp4')

while True:
    #이 read는 VideoCapture.read()의 기능으로 grab 과 retrieve 함수 두 개를 수행함.
    #grab은 영상의 다음 프레임을 감지하는 기능
    #retrieve는 영상을 decode하고 bool값을 return함. 안됐다면 false를 반환하겠지.
    ret, frame = capt.read()
    #print(type(ret)) <class 'bool'> 이미지를 불러왔음을 확인할 수 있는 변수 // 가 맞았네 그러면
    #print(type(frame)) <class 'numpy.ndarray'> 처리할 img // 처리할 프레임의 데이터는 frame이 가지고 있게 되는 것이지.
    #출:http://sonsondiary.tistory.com/entry/영상처리-Capture-Video-from-Camera 에 번역 해주셔서 쉽게 봤음.
    
    #ret가 true를 반환할 때, 즉 영상을 불러오는데 성공했을 때
    if ret:
        #영상을 bgr로 받아오니 rgb로 convert하고 부르는 것.
        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        cv2.imshow('frame',rgb)
        #print(type(rgb)) #<class 'numpy.ndarray'> 가공된 프레임의 데이터가 들어있는 곳이지.

        #q를 누르면 꺼진다 이말임. waitkey는 키보드를 누를 때 까지 괄호 안의 숫자*ms의 시간 동안 대기하라는 말임. & 0xFF (10진수 255) 를 넣는 이유는
        #64비트 운영체제에서 사용하기 위함임. 우리가 수를 누르면 이제 그 키의 정수형 값을 반환함.
        #q의 경우 113이었음. 대기시간이니까 이를 이용하면 프레임을 조절할 수 있을 것임.(실제로 1로하니 2s만에 끝나고 10으로 하니 8s 정도 걸림)
        #552frame이 전체 영상이 가진 프레임 양 , 초당 24frame이 지나가고 23s짜리 영상이었음
        #프레임 넘어가는 동안 n ms를 기다리게 되는데, 1ms = 0.001s 니까, 프레임 하나당 0.04167s가 걸리고 41.67 ms 일 거야.
        #42로 하니 26s 걸리는 것을 보고 41로 하니 25s 걸림
        if cv2.waitKey(40) & 0xFF == ord('q'):
            break

    #영상을 불러오지 못했다면... 꺼지는 것이야
    else: 
        break
#의문 : 왜 ret은 boolean이고 gray, frame은 안에서는 둘다 ndarray인데 왜 
#나와서는 gray만 ndarray인가. -> 해결, 그 이유는 read()라는 함수의 특징인 grab과 retrieve를 하는 것 때문
#ret와 frame의 순서를 바꿔보니 frame이 boolean이 되는걸 보니 순서대로 retrieve, grab되는 변수가 되는 것임.


#release는 video를 닫는다는 명령어
capt.release()

#윈도우 제거 -> 키보드 누르면 꺼진다 라는 말.
cv2.destroyAllWindows()
